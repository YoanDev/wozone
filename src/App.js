import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './pages';
import Signin from './pages/signin';
import ScrollToTop from './components/ScrollToTop'

function App() {
  return (
    <>
    <Router>

    <ScrollToTop />

        <Switch>

          <Route path='/' component={Home} exact />
          <Route path='/signin' component={Signin} exact />
        </Switch>



    </Router>
    </>
  );
}

export default App;
