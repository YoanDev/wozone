import React from 'react'
import Icon1 from '../../images/service1.svg'
import Icon2 from '../../images/service2.svg'
import Icon3 from '../../images/service4.svg'
import { ServicesCard, ServicesContainer, ServicesH1, ServicesH2, ServicesIcon, ServicesP, ServicesWrapper } from './OffersElements.jsx'


const Offers = () => {
    return (
        <ServicesContainer id='offers'>
            <ServicesH1>Offers</ServicesH1>
            <ServicesWrapper>
                <ServicesCard>
                    <ServicesIcon src={Icon1}/>
                    <ServicesH2>25$/month during 3 months</ServicesH2>
                    <ServicesP>Standart offer 3 months</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon2}/>
                    <ServicesH2>15$/month during 1 year</ServicesH2>
                    <ServicesP>Standart offer 1 year</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon3}/>
                    <ServicesH2>20$/month during 1 year</ServicesH2>
                    <ServicesP>Premium offer 1 year</ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default Offers
