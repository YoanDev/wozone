import Bg1 from '../../images/bg1.svg'
import Bg2 from '../../images/nutrition.svg'
import Bg3 from '../../images/sign.svg'
import Bg4 from '../../images/offers.svg'
import coaching from '../../images/coach.svg'
import Bgtraining from '../../images/train.svg'

export const homeObjOne = {
    id: 'about',
    lightBg:false,
    lightText:true,
    lightTextDesc:true,
    topLine:'Gym',
    description:'Get access to our gym with high qualited equipments',
    buttonLabel: 'Get Access',
    imgStart: false,
    img: Bg1,
    alt:'car',
    dark:true,
    primary:true,
    darkText: false,
} 
export const homeObjTwo = {
    id: 'training',
    lightBg:true,
    lightText:false,
    lightTextDesc:false,
    topLine:'Training',
    headline: 'Train, eat, sleep, repeat',
    description:'we promise you to teaching you how to train while minimizing the risk of injury',
    buttonLabel: 'Learn More',
    imgStart: true,
    img: Bgtraining,
    alt:'training',
    dark:false,
    primary:false,
    darkText: true,
} 
export const homeObjThree = {
    id: 'services',
    lightBg:false,
    lightText:true,
    lightTextDesc:true,
    topLine:'Services',
    headline: 'Check out our Services',
    description:'Get access to our exclusive app that allows you to send unlimited transactions without getting charged any fees.',
    buttonLabel: 'Get Started',
    imgStart: false,
    img: Bg3,
    alt:'car',
    dark:true,
    primary:true,
    darkText: false,
} 

export const homeObjFour = {
    id: 'nutrition',
    lightBg:true,
    lightText:false,
    lightTextDesc:false,
    topLine:'In the Kitchen',
    headline: 'All you have to know about nutrition',
    description:'One of the big three to build muscle',
    buttonLabel: 'See more',
    imgStart: false,
    img: Bg4,
    alt:'cook',
    dark:false,
    primary:false,
    darkText: true,
} 

export const homeObjFive = {
    id: 'coaching',
    lightBg:true,
    lightText:false,
    lightTextDesc:false,
    topLine:'Coaching',
    headline: 'Engage a coach to maximize your gains',
    description:'Our coachs give you a program adapted on your need based on your body, a unique program for a unique person',
    buttonLabel: 'See more',
    imgStart: true,
    img: coaching,
    alt:'car2',
    dark:false,
    primary:false,
    darkText: true,
} 


