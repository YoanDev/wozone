import React from 'react'
import Icon1 from '../../images/service1.svg'
import Icon2 from '../../images/service2.svg'
import Icon3 from '../../images/service4.svg'
import { ServicesCard, ServicesContainer, ServicesH1, ServicesH2, ServicesIcon, ServicesP, ServicesWrapper } from './ServicesElements'


const Services = () => {
    return (
        <ServicesContainer id='services'>
            <ServicesH1>Our Services</ServicesH1>
            <ServicesWrapper>
                <ServicesCard>
                    <ServicesIcon src={Icon1}/>
                    <ServicesH2>Personals trainers</ServicesH2>
                    <ServicesP>Let our coaches guide you to become the best version of yourself</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon2}/>
                    <ServicesH2>Programs based on your body</ServicesH2>
                    <ServicesP>You can access our plateform online anywhere in the world</ServicesP>
                </ServicesCard>
                <ServicesCard>
                    <ServicesIcon src={Icon3}/>
                    <ServicesH2>Premium Members</ServicesH2>
                    <ServicesP>Come with one guest for free</ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default Services
