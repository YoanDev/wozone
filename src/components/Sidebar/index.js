import React from 'react'
import {CloseIcon, Icon, SidebarContainer, SidebarRoute, SideBtnWrap, SidebarWrapper, SidebarLink, SidebarMenu} from './SidebarElements.jsx'

const Sidebar = ({isOpen, toggle}) => {
    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
            <Icon onClick={toggle}>
                <CloseIcon />
            </Icon>
            <SidebarWrapper>
                <SidebarMenu>
                    <SidebarLink  to='about' onClick={toggle}>
                        About
                    </SidebarLink>
                    <SidebarLink to='training' onClick={toggle}>
                        Training
                    </SidebarLink> 
                    <SidebarLink to='nutrition' onClick={toggle}>
                        Nutrition
                    </SidebarLink>
                    <SidebarLink to='offers' onClick={toggle}>
                        Offers
                    </SidebarLink>
                </SidebarMenu>
                <SideBtnWrap>
                    <SidebarRoute to='/signin' >Sign In</SidebarRoute>
                </SideBtnWrap>
            </SidebarWrapper>
        </SidebarContainer>
    )
}

export default Sidebar
