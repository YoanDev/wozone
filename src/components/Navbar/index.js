import React from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavLogo, NavMenu } from './NavbarElements'
import {FaBars} from 'react-icons/fa'

const Navbar = ({toggle}) => {
    

    return (
        <>
        <Nav>
            <NavbarContainer>
                <NavLogo to='/'>Wozone</NavLogo>
                <MobileIcon onClick={toggle}>
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='about'smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='training' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Training</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='services' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Services</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='coaching' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Coaching</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='offers' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Offers</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='nutrition' smooth={true} duration={500} spy={true} exact='true' offset={-80}>Nutrition</NavLinks>
                    </NavItems>
                        
                </NavMenu>

                <NavBtn>
                    <NavBtnLink to='/signin'>Sign In</NavBtnLink>
                </NavBtn>
            </NavbarContainer>
        </Nav>
        </>
    )
}

export default Navbar
