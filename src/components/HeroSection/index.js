import React, { useState } from 'react'
import Video from '../../videos/vid1.mp4'
import Button from '../ButtonElement'
import { ArrowForward, ArrowRight, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, VideoBg, HeroBtnWrapper } from '././HeroElements.jsx'

const HeroSection = () => {
    const [hover, setHover] = useState(false)

    const onHover = () => {
        setHover(!hover)
    }
    return (
        <HeroContainer>
            <HeroBg>
                <VideoBg autoPlay loop muted src={Video} type='video/mp4' />
            </HeroBg>
            <HeroContent>
                <HeroH1>Eat sport, live sport, sleep sport. Enjoy the grind</HeroH1>
                <HeroP>
                    Get a suscription the {new Date().getDate()}/{new Date().getMonth()}/{new Date().getFullYear()} and receive 2 month of trial.
                </HeroP>
                <HeroBtnWrapper>

                    <Button to='signup' onMouseEnter={onHover}
                     onMouseLeave={onHover}
                     primary = 'true'
                     dark='true'
                     smooth={true} duration={500} spy={true} exact='true' offset={-80} 
                     >
                        Get started {hover ? <ArrowForward /> : <ArrowRight />}
                    </Button>

                </HeroBtnWrapper>
            </HeroContent>
        </HeroContainer>
    )
}

export default HeroSection
